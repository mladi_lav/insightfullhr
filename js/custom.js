$(document).ready(function () {

    $('[ data-toggle-menu]').on('click', function () {
        var btn = $(this),
            aside = $('aside'),
            isActive = btn.hasClass('active');
      btn[isActive ? 'removeClass' : 'addClass']('active');
      aside[isActive ? 'removeClass' : 'addClass']('active');
    });

    if ($('.slider').length > 0) slider.init();

    $('.form-control').on('keydown', function () {
        var value = $(this).val();
        if(value.length > 0){
            if($(this).parent().find('.clear-input').length == 0)
                $(this).parent().append('<div class="clear-input"></div>');
        } else {
            $(this).parent().find('.clear-input').remove();
        }
    });

    $(document).on('click', '.clear-input', function () {
        $(this).siblings('input').val('');
        $(this).remove();
    });

    video();




});




function video() {
    if($('#bgvid').length > 0) {
        var vid = document.getElementById("bgvid"),
            pauseButton = document.getElementById("vidpause");

        function vidFade() {
            vid.classList.add("stopfade");
            pauseButton.classList.add("stop");
            $('.item.active').removeClass('play-video');
        }

        vid.addEventListener('ended', function () {
            vid.pause();
            vidFade();
        });

        $('.close-video').on('click', function () {
            vid.pause();
            vidFade();
        });

        pauseButton.addEventListener("click", function () {
            vid.classList.toggle("stopfade");
            pauseButton.classList.toggle("stop");

            var wrapper = $('.item.active'),
                isPlay = wrapper.hasClass('play-video');

            wrapper[!isPlay ? 'addClass' : 'removeClass']('play-video');

            if (vid.paused)
                vid.play();
            else
                vid.pause();
        });
    }
}


var isFirstLoad = true;
var sliderInner = {

    options: {},


    setOptions: function () {
        this.options.activeItem = '.item.active';
        this.options.$slider = $(this.options.activeItem + ' .slider-inner');
        this.options.$container = $(this.options.activeItem + ' .slider-wrapper');
        this.options.$items = $(this.options.activeItem + ' .slider-inner').find('.slide');
        this.options.count = this.options.$items.length;
        this.options.width = this.options.$slider.width();
        this.options.paginationClass = 'inner-slider-navigation';
        this.options.pagenationStyledClass = 'slider-navigation';
    },

    init: function () {
        this.setOptions();
        if(this.options.count > 0) {
            this.options.$slider.width(this.options.width);
            this.options.$container.width(this.options.width * this.options.count);
            this.options.$container.css({display: 'flex'});

            this.navigation();


            this.bind();
        }
    },

    bind: function () {
        if(isFirstLoad) {

            $(document).on('click', this.options.activeItem + ' .' + this.options.paginationClass + ' li', function (e) {
                if ($(this).hasClass('active')) return false;

                var li = $(this),
                    index = li.index(),
                    isFirst = index == 0,
                    isLast = index == sliderInner.options.count - 1;

                li.addClass('active').siblings('li').removeClass('active');

                sliderInner.options.$container.css({marginLeft: '-' + index * sliderInner.options.width + 'px'});

                $(sliderInner.options.activeItem + ' .prev')[isFirst ? 'removeClass' : 'addClass']('active');
                $(sliderInner.options.activeItem + ' .next')[isLast ? 'removeClass' : 'addClass']('active');

            });



            $(document).on('click', '.next', function () {
                sliderInner.next();
            });

            $(document).on('click', '.prev', function () {
                sliderInner.prev();
            });

            isFirstLoad = false;
        }
    },

    next: function () {
        $(this.options.activeItem + ' .' + this.options.paginationClass + ' li.active').next('li').click();
    },

    prev: function () {
        $(this.options.activeItem + ' .' + this.options.paginationClass + ' li.active').prev('li').click();
    },


    navigation: function () {

        if(this.options.count > 1) {

            $('.' + this.options.paginationClass).remove();
            var navigation = document.createElement('ul');
            navigation.className = this.options.pagenationStyledClass + ' ' + this.options.paginationClass;

            for (var i = 0; i < this.options.count; i++) {
                var li = document.createElement('li');
                navigation.appendChild(li);
            }

            this.options.$slider.append(navigation);
            this.options.$slider.find('.' + this.options.paginationClass +' li').eq(0).addClass('active');


            this.initButtons();
        }

    },
    initButtons: function () {

        $('.prev').remove();
        $('.next').remove();
        var next = document.createElement('div'),
            prev = document.createElement('div');

        next.className = 'next active';
        prev.className = 'prev';

        this.options.$slider.append(next);
        this.options.$slider.append(prev);
    },
}


var slider = {
    $slider: $('.slider'),
    $items: $('.slider').find('.item'),
    count: $('.slider').find('.item').length,
    paginationClass: 'main-slider-navigation',
    pagenationStyledClass: 'slider-navigation',

    init: function () {

        this.navigation();
        this.$items.eq(0).addClass('active');
        sliderInner.init();

    },

    next: function () {
        $('.' + slider.paginationClass +' li.active').next('li').click();
    },

    prev: function () {
        $('.' + slider.paginationClass +' li.active').prev('li').click();
    },

    navigation: function () {

        if(this.count > 1) {
            var navigation = document.createElement('ul');
            navigation.className = slider.pagenationStyledClass + ' ' + slider.paginationClass;

            for (var i = 0; i < this.count; i++) {
                var li = document.createElement('li');
                navigation.appendChild(li);
            }

            this.$slider.append(navigation);
            $('.main-slider-navigation li').eq(0).addClass('active');



            $(document).on('click', '.' + slider.paginationClass + ' li', function (e) {
                if ($(this).hasClass('active')) return false;

                var li = $(this),
                    index = li.index(),
                    isFirst = index == 0,
                    isLast = index == slider.count - 1;

                li.addClass('active').siblings('li').removeClass('active');
                slider.$items.eq(index).addClass('active').siblings('.item').removeClass('active').css({top: '100%'});
                slider.$items.eq(index).prev('.item').css({top: '-100%'});

                $('.up')[isFirst ? 'removeClass' : 'addClass']('active');
                $('.down')[isLast ? 'removeClass' : 'addClass']('active');

                sliderInner.init();
            });

            this.initButtons();
            this.initKey();
        }

    },
    initButtons: function () {
        var down = document.createElement('div'),
            up = document.createElement('div');

        down.className = 'down active';
        up.className = 'up';


        var icon = document.createElement('i');
        icon.className = 'icon-down';
        down.appendChild(icon);
        this.$slider.append(down);

        var icon = document.createElement('i');
        icon.className = 'icon-down';
        up.appendChild(icon);
        this.$slider.append(up);

        $('.down').on('click', function () {
            slider.next();
        });

        $('.up').on('click', function () {
            slider.prev();
        });

    },
    initKey: function () {
        $('html').on('keydown', function (e) {
            switch (e.keyCode){
                case 40:
                    slider.next();
                    break;
                case 38:
                    slider.prev();
                    break;
            }
        })
    }
};


var map;
function initMap() {
    var customMapType = new google.maps.StyledMapType([
        {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
                { "saturation": -100 },
                { "lightness": -8 },
                { "gamma": 1.18 }
            ]
        }, {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                { "saturation": -100 },
                { "gamma": 1 },
                { "lightness": -24 }
            ]
        }, {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                { "saturation": -100 }
            ]
        }, {
            "featureType": "administrative",
            "stylers": [
                { "saturation": -100 }
            ]
        }, {
            "featureType": "transit",
            "stylers": [
                { "saturation": -100 }
            ]
        }, {
            "featureType": "water",
            "elementType": "geometry.fill",
            "stylers": [
                { "color": "#cad2d4" }
            ]
        }, {
            "featureType": "road",
            "stylers": [
                { "saturation": -100 }
            ]
        }, {
            "featureType": "administrative",
            "stylers": [
                { "saturation": -100 }
            ]
        }, {
            "featureType": "landscape",
            "stylers": [
                { "saturation": -100 }
            ]
        }, {
            "featureType": "poi",
            "stylers": [
                { "saturation": -100 }
            ]
        }, {}
    ], {
        name: 'Custom Style'
    });
    var customMapTypeId = 'custom_style';

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom:5,
        scrollwheel: false,
        center: {lat: 49.9939346, lng: 36.2407475},  // Brooklyn.
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, customMapTypeId]
        }
    });

    map.mapTypes.set(customMapTypeId, customMapType);
    map.setMapTypeId(customMapTypeId);

    var image = 'images/label.png';
    var beachMarker = new google.maps.Marker({
        position: {lat:  49.99393, lng:  36.24074},
        map: map,
        icon: image
    });


}